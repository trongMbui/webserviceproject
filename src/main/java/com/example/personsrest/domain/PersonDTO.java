package com.example.personsrest.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

import java.util.List;

@Value
public class PersonDTO {

    String id;
    String name;
    int age;
    String city;
    boolean active;
    List <String> groups;



}
