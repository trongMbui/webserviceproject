package com.example.personsrest.domain;

import com.example.personsrest.remote.GroupRemote;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RequestMapping( "/api/persons/")
@RestController
@AllArgsConstructor
public class PersonController {

    PersonService personService;
    GroupRemote groupRemote;

   @GetMapping
    public List<PersonDTO> all(String search, String pagesize, String pagenumber){

       return personService.all().map(this::toDTO).collect(Collectors.toList());

   }

   @PostMapping
   public PersonDTO createPerson(@RequestBody CreatePerson createPerson){
       return toDTO(personService.createPerson(
               createPerson.getName(),
               createPerson.getAge(),
               createPerson.getCity()
       ));
   }

   @GetMapping("/{id}")
   public PersonDTO get(@PathVariable("id") String id){
       return toDTO(personService.get(id));
   }

   @PutMapping("/{id}")
   public PersonDTO updatePerson(@PathVariable String id, @RequestBody UpdatePerson updatePerson){
    return toDTO(personService.updatePerson(
            id,
            updatePerson.getName(),
            updatePerson.getAge(),
            updatePerson.getCity()
            ));
   }

   @DeleteMapping("/{id}")
   public void deletePerson(@PathVariable("id") String id){
         personService.delete(id);
   }

   @PutMapping("/{id}/addGroup/{name}")
   public PersonDTO addGroup(@PathVariable("id") String id, @PathVariable("name") String name){
       return toDTO(personService.addGroupToPerson(id, name));
   }

    @DeleteMapping("/{id}/removeGroup/{name}")
    public PersonDTO addGroup(String personId, String groupName){
       return null;
    }



   private PersonDTO toDTO(Person person ){
       return new PersonDTO(
               person.getId(),
               person.getName(),
               person.getAge(),
               person.getCity(),
               true,
               person.getGroups().stream().map(s -> groupRemote.getNameById(s)).collect(Collectors.toList())
       );
   }
}
