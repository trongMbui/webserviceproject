package com.example.personsrest.remote;

import com.example.personsrest.domain.Person;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.UUID;


public class GroupRemoteImpl implements GroupRemote {


    @Override
    public String getNameById(String groupId) {
        return null;
    }

    @Override
    public String createGroup(String name) {
        return null;
    }

    @Override
    public String removeGroup(String name) {
        return null;
    }

    WebClient webClient;

    public GroupRemoteImpl() {
        webClient = WebClient.builder()
                .baseUrl("https://groups.edu.sensera.se/api/groups")
                .build();
    }

    public Group get(String name){
        return webClient.get()
                .uri("posts/" + name)
                .retrieve()
                .bodyToMono(Group.class)
                .block();
    }

    @Value
    public static class Group{
        String id;
        String name;

        Person person;


        public Group( String name, Person person) {
            this.id = UUID.randomUUID().toString();
            this.name = name;
            this.person = person;
        }
    }

}
